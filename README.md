# gulp-up-cdn

## 概述 Summary

该工具允许你通过Gulp管道将文件上传到CDN (七牛云, 阿里云 OSS, 亚马逊 S3)。

This tool allows you to upload files to CDN (Qniuyun, Alicloud OSS, Amazon S3) via Gulp pipe.

## 用法 Usage

### 上传

```javascript
const cdn = require('gulp-up-cdn')

// Qiniu Cloud
gulp.task('taskname', function(){
  return gulp.src('filePath').pipe(cdn('qn', {
    accessKey: '',
    secretKey: '',
    bucket: 'bucket-name',
    // 非必填，用于log输出全路径
    // This parameter is optional. Log output full path
    prefix: "https://xxx"
  }, 'folderPath/fileName.suffix'))
});

// Ali Cloud OSS
gulp.task('taskname', function(){
  return gulp.src('filePath').pipe(cdn('oss', {
    accessKeyId: '',
    accessKeySecret: '',
    bucket: 'bucket-name',
    region: 'oss-region',
    // 非必填，用于log输出全路径
    // This parameter is optional. Log output full path
    prefix: 'https://xxx'
  }, 'folderPath/fileName.suffix'))
});

// Amazon S3
gulp.task('taskname', function(){
  return gulp.src('filePath').pipe(cdn('s3', {
    accessKeyId: '',
    secretAccessKey: '',
    Bucket: 'bucket-name',
    region: 's3-region',
    // 非必填，用于log输出全路径
    // This parameter is optional. Log output full path
    prefix: 'https://xxx'
  }, 'folderPath/fileName.suffix'))
});
```